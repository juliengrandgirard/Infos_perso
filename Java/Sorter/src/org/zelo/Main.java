package org.zelo;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Creation + Sort d'une liste d'Integer
		
		List<Integer> listeDeInteger = new ArrayList<Integer>();

		//Avant le sort
		System.out.println("Avant le Sort de la Liste<Integer> : ");
		for(int i = 0; i < 100; i++) {
			listeDeInteger.add((int)(Math.random()*10 + 1));
			if(i % 10 == 0 && i != 0)
				System.out.println(listeDeInteger.get(i));
			else
				System.out.print(listeDeInteger.get(i) + " ");
		}
		
		System.out.println("");
		System.out.println("");
		Collections.sort(listeDeInteger);
		
		// Apres le sort
		System.out.println("Apr�s le Sort de la Liste<Integer> : ");
		for(int i = 0; i < listeDeInteger.size(); i++) {
			if(i % 10 == 0 && i != 0)
				System.out.println(listeDeInteger.get(i));
			else
				System.out.print(listeDeInteger.get(i) + " ");
		}
		
		System.out.println("");
		System.out.println("");
		System.out.println("");
		
		// Creation d'une fonction pour trier une liste de Personne
		
		List<Personne> listeDePersonne = new ArrayList<Personne>();

		listeDePersonne.add(new Personne("Yann"));
		listeDePersonne.add(new Personne("Julien"));
		listeDePersonne.add(new Personne("Robin"));
		listeDePersonne.add(new Personne("Julien"));
		
		System.out.println("Avant le Sort de la Liste<Personne> : ");
		for(int i = 0; i < listeDePersonne.size(); i++) {
			System.out.print(listeDePersonne.get(i).getName() + " ");
		}
		
		TriagePersonne.sortListPersonne(listeDePersonne);
		System.out.println("");
		System.out.println("");
		
		// Apres le sort
		System.out.println("Apr�s le Sort de la Liste<Personne> : ");
		for(int i = 0; i < listeDePersonne.size(); i++) {
			System.out.print(listeDePersonne.get(i).getName() + " ");
		}
	}

}
