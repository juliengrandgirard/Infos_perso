package org.zelo;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TriagePersonne {
	
	public static void sortListPersonne(List<Personne> listPersonne) {
		Collections.sort(listPersonne, new Comparator<Personne>(){
			@Override
			public int compare(Personne p1, Personne p2) {
				if(p1 == null && p2 == null) {
					return 0;
				} 
				if(p1 == null) {
					return -1;
				}
				if(p2 == null) {
					return +1;
				}
				
				return p1.getName().compareTo(p2.getName());
			}
		});
	}
}
