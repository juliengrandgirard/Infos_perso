package org.zelo;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService()
public interface IMonInterface {
	@WebMethod
	public String getName();
	@WebMethod
	public void setName(String name);
}
