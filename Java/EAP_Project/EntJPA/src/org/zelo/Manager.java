package org.zelo;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Manager
 *
 */
@Entity
@Table(name="MANAGERS")
public class Manager implements Serializable {

	@Id
	@Column(name="idM")
	private long id;
	
	@Column(name="nom")
	private String name;
	private static final long serialVersionUID = 1L;
	
	@OneToMany(mappedBy="manager")
	private List<Employee> employee;

	public Manager() {
		super();
	}   
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Employee> getEmployee() {
		return employee;
	}
	
	public void setEmployee(List<Employee> employee) {
		this.employee = employee;
	}
   
}
