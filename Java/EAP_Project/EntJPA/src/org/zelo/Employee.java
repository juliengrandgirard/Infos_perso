package org.zelo;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MaJPA
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="findAllEmployee", query="Select e from Employee e")
})
@Table(name="EMPLOYEES")
public class Employee implements Serializable {

	@Id
	@Column(name="idE")
	private Long id;
	
	@Column(name="nom")
	private String name;
	private static final long serialVersionUID = 1L;
	
	@ManyToOne()
	@JoinColumn(name="MANAGER", referencedColumnName="idM")
	private Manager manager;

	public Employee() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}
