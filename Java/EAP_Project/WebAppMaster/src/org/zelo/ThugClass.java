package org.zelo;

import javax.ejb.EJB;
import javax.jws.WebService;

@WebService(serviceName = "JaxRI", portName= "JaxRI", endpointInterface = "org.zelo.IThugClass")
public class ThugClass implements IThugClass {
	
	@EJB
	UnAutreServiceLocal uas;
	
	@Override
	public String getName() {
			return uas.getMessage();
	}
	
	@Override
	public void setName(String Nname) {
		
	}
}
