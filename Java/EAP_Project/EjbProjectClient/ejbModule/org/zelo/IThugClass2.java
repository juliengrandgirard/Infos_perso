package org.zelo;

import javax.ejb.Local;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
@Local
public interface IThugClass2 {
	@WebMethod
	public String getName();
	@WebMethod
	public void setName(String name);
}
