package org.zelo;

import javax.ejb.Local;

@Local
public interface UnAutreServiceLocal {
	void message();
	public String getMessage();
}
