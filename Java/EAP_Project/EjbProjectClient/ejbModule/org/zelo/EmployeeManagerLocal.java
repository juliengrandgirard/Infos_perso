package org.zelo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface EmployeeManagerLocal {
	public List<Employee> findAllEmployee();
}
