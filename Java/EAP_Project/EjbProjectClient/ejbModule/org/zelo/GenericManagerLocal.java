package org.zelo;

import javax.ejb.Local;

@Local
public interface GenericManagerLocal {
	public <T> T create(T t);
	public <T> T update(T t);
	public <T> void delete(T t);
}
