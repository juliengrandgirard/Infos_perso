package org.zelo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class UnAutreService
 */
@Stateless
@LocalBean
public class UnAutreService implements UnAutreServiceRemote, UnAutreServiceLocal {

    /**
     * Default constructor. 
     */
    public UnAutreService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void message() {
		// TODO Auto-generated method stub
		System.out.println("UnAutreService -> Je suis un sous service !!");
	}
	
	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "UnAutreService -> Je suis un sous service !!";
	}

}
