package org.zelo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class GenericManager
 */
@Stateless
@LocalBean
public class GenericManager implements GenericManagerRemote, GenericManagerLocal {

    /**
     * Default constructor. 
     */
    public GenericManager() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public <T> T create(T t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T update(T t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> void delete(T t) {
		// TODO Auto-generated method stub
	}

}
