package org.zelo;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class MyEjbSession
 */
@Stateless
@LocalBean
public class MyEjbSession implements MyEjbSessionRemote, MyEjbSessionLocal {
	
	@EJB
	UnAutreService s;

    /**
     * Default constructor. 
     */
    public MyEjbSession() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public float calcule(float valeur) {
		// TODO Auto-generated method stub
		s.message();
		return 0;
	}

}
