package org.zelo;

public class Singleton {
	
	private static Singleton instance;
	private static final String mutex = Singleton.class.getName();
	private static int counter;
	
	// Comme le contructeur mais une seule et unique fois pour la class
	static {
		counter = 0;
	}
	
	// ici le private empeche l'instentiation de la class pour qu'elle soit g�r� dans le getInstance()
	private Singleton () {
		//
	}

	// On cr�e une instance global grace au static et on empeche 2 personnes d'acc�der a l'instantiation avec une mutex
	public static Singleton getInstance() {
		synchronized(mutex){
			if (instance == null)
				instance = new Singleton();
		}
		return instance;
	}

	public static void setInstance(Singleton instance) {
		Singleton.instance = instance;
	}
}
