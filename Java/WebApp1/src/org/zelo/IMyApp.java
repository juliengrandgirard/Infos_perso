package org.zelo;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService()
public interface IMyApp {
	@WebMethod
	public String getName();
	@WebMethod
	public void setName(String name);
}
