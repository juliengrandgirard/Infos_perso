<?php
/**
 * Facebook Connect enable to use Facebook API
 *
 * @author Florian BLARY <fblary@sqli.com>
 */

namespace AppBundle\Utils;

use Facebook;

use AppBundle\Entity\Agency;
use AppBundle\Entity\Image;
use AppBundle\Entity\Post;
use Doctrine\ORM\EntityManager;

use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Bramus\Monolog\Formatter\ColoredLineFormatter;

class FacebookConnect
{
    private $fb;
    private $accessToken;
    private $container;
    private $em;
    private $translator;

    const FB_ERROR_PAGE_LIMIT_RATE = 32;

    /**
     * Initialize
     */
    public function __construct(Container $container, EntityManager $entityManager, Translator $translator)
    {
        $this->translator = $translator;
        $this->container = $container;
        $this->em = $entityManager;
        $this->accessToken = $container->getParameter('facebook_app_access_token');
        $this->fb = new Facebook\Facebook([
            'app_id' => $container->getParameter('facebook_app_id'),
            'app_secret' => $container->getParameter('facebook_app_secret'),
            'default_graph_version' => $container->getParameter('facebook_app_default_graph_version')
        ]);
    }

    /**
     * Return the Facebook page token for the given page
     *
     * @param $pageId
     * @return mixed
     * @throws \Exception
     */
    private function getPageToken($pageId)
    {
        try {
            $page = json_decode($this->fb->get($pageId."?fields=access_token", $this->accessToken)->getBody());
            return $page->access_token;
        } catch (\Exception $e) {
            $this->logError($e, 'getPageToken', 'facebook/get_page_token.log');
            throw $e;
        }
    }

    /**
     * Return an array with every post on the given page
     *
     * @param $pageId
     * @param string $options
     *
     * @return bool
     */
    public function getPostsOnPage($pageId,$options = "")
    {
        try {
            $allPosts = json_decode($this->fb->get('/' . $pageId . '/feed'.$options, $this->getPageToken($pageId))->getBody());
            return $allPosts->data;
        } catch (\Exception $e) {
            $this->logError($e, 'getPostsOnPage', 'facebook/get_posts_on_page.log');
            return false;
        }
    }

    /**
     * Get details of one particular post
     *
     * @param $postId
     * @param $pageId
     * @return array|bool
     */
    public function getPostDetails($postId, $pageId)
    {
        try {
            $postDetails = json_decode($this->fb->get('/' . $postId . '/attachments', $this->getPageToken($pageId))->getBody());
            return $postDetails->data;
        } catch (\Exception $e) {
            $data = [
                'Post ID : '.$postId,
                'Page ID : '.$pageId
            ];
            $this->logError($e, 'getPostDetails', 'facebook/get_posts_details.log', $data);
            return false;
        }
    }

    /**
     * Publish a post on the given page
     *
     * @param Post $post
     * @param bool|false $postNow
     *
     * @return bool
     */
    public function postOnPage(Post $post, $postNow = false)
    {
        $token = null;
        try {
            $now = new \DateTime();
            if($postNow || $post->getScheduledTime() <= $now) {

                if(strpos($post->getLink(), '?')) {
                    $link = $post->getLink().$this->container->getParameter('url_traking');
                } else {
                    $link = $post->getLink()."?".$this->container->getParameter('url_traking');
                }

                $token = $this->getPageToken($post->getAgency()->getPageId());
                $postData = [
                    'link' => $link,
                    'name' => $post->getTitle(),
                ];

                if ( $agencyDescription = $post->getAgency()->getDescription() ) {
                    $postData['description'] = $agencyDescription;
                }

                if ( $imagePath = $post->getImagePath( $this->container->getParameter('activity_domain_img_path') ) ) {
                    $postData['picture'] = $this->container->getParameter('base_url') . $imagePath;
                }

                $publication = $this->fb->post(
                    '/' . $post->getAgency()->getPageId() . '/feed',
                    $postData, $token
                );

                if($publication != null ) {
                    $facebookPostData = json_decode($publication->getBody());

                    if( isset($facebookPostData->id)) {
                        $facebookPostData = json_decode($publication->getBody());

                        $post->setFacebookPostId($facebookPostData->id);
                        $post->setPostedOn(new \DateTime());
                        $post->setPostOnFacebook(true);

                        // update firstPostOnFacebook (Offer)
                        $offer = $post->getOffer();
                        if(!$offer->getFirstPostOnFacebook()) {
                            $offer->setFirstPostOnFacebook(new \DateTime());
                            $this->em->persist($offer);
                        }

                        $this->em->persist($post);
                        $this->em->flush();
                    }
                }
            }
        } catch (\Exception $e) {
            $data = [
                'Post ID : '.$post->getId(),
                'Token: '.($token ?: 'null')
            ];
            $this->logError($e, 'postOnPage', 'facebook/post_on_page.log', $data);
            return false;
        }
        return true;
    }

    /**
     * Delete the old post and publish the new one
     *
     * @param Post $post
     * @return bool
     */
    public function updatePostOnPage(Post $post)
    {
        $this->deletePostOnPage($post);

        // update post entity
        $post->setPostOnFacebook(false);
        $post->setFacebookPostId(null);
        $post->setPostedOn(null);
        $post->setIsAuto(false);
        $this->em->persist($post);
        $this->em->flush();

        try {
            $this->postOnPage($post, false);
        } catch (\Exception $e) {
            $data = [
                'Post ID : '.$post->getId()
            ];
            $this->logError($e, 'updatePostOnPage', 'facebook/update_post_on_page.log', $data);
            return false;
        }
        return true;
    }

    /**
     * Delete the post on the given page
     *
     * @param Post $post
     * @return mixed
     */
    public function deletePostOnPage($post)
    {
        try {
            $this->fb->delete('/' . $post->getFacebookPostId(), array(), $this->getPageToken($post->getAgency()->getPageId()));
        } catch (\Exception $e) {
            $data = [
                'Post ID : '.$post->getId()
            ];
            $this->logError($e, 'deletePostOnPage', 'facebook/delete_post_on_page.log', $data);
            return false;
        }
        return true;
    }

    /**
     * Delete the post on the given page from facebook ids
     *
     * @param Post $post
     * @return mixed
     */
    public function deleteFacebookPostOnPage($postId, $pageId)
    {
        try {
            return $this->fb->delete('/' . $postId, array(), $this->getPageToken($pageId));
        } catch (\Exception $e) {
            $data = [
                'Page ID : ' .$pageId,
                'Post ID : ' .$postId
            ];
            $this->logError($e, 'deleteAllPosts', 'facebook/delete_all_posts.log', $data);
            return false;
        }
    }

    /**
     * Prepare Facebook request with given form elements.
     *
     * @param Agency $agency
     * @param Image|null $cover
     * @param Image|null $picture
     * @param $phone
     * @param $website
     * @param $description
     */
    public function updatePage(Agency $agency, Image $cover = null, Image $picture = null, $phone, $website, $description )
    {
        try {
            $pageId = $agency->getPageId();

            // # 1st request # to get page token
            $pageToken = $this->getPageToken( $pageId );

            // Page element to update
            $params = [];

            if ( !empty($cover) ) {
                // # 2nd and 3rd requests # to create facebook album if not created yet and add cover to album
                $facebookCover = $this->addCoverToAssetsAlbum( $agency, $cover, $pageToken );
                if ( $facebookCover ) {
                    $params['cover'] = $facebookCover->id;
                }
            }
            if ( !empty($phone) ) {
                $params['phone'] = $phone;
            }
            if ( !empty($website) ) {
                $params['website'] = $website;
            }
            if ( !empty($description) ) {
                $params['description'] = $description;
            }

            // Stack requests
            $batchRequests = [];

            // Profile picture
            if ( !empty($picture) ) {
                $batchRequests['update-picture'] = $this->fb->request(
                    'POST',
                    '/'.$pageId.'/picture',
                    ['picture' => $this->container->getParameter('base_url').$picture->getWebPath()],
                    $pageToken
                );
            }

            // Page info
            if ( !empty($params) ) {
                $batchRequests['update-page'] = $this->fb->request(
                    'POST',
                    '/'.$pageId,
                    $params,
                    $pageToken
                );
            }

            if ( !empty($batchRequests) ) {
               $this->sendBatchRequests( $batchRequests, $params, $agency, $picture );
            }
        } catch (\Exception $e) {
            $data = [
                'Agency ID : '.$agency->getId()
            ];
            $this->logError($e, 'updatePage', 'facebook/update_page.log', $data);
            $this->addFlashBag('error', 'result.page.error.update', array('%name%' => $agency->getName()));
        }
    }

    /**
     * Send Facebook requests and deal with success and errors.
     *
     * @param array $batchRequests
     * @param array $params
     * @param Agency $agency
     * @param Image|null $picture
     * @throws \Exception
     */
    private function sendBatchRequests(array $batchRequests, array $params, Agency $agency, Image $picture = null)
    {
        // Send requests all together
        $this->fb->setDefaultAccessToken($this->accessToken);

        try {
            $responses = $this->fb->sendBatchRequest( $batchRequests );
        }
        catch (\Exception $e) {
            throw $e;
        }

        /** @var Facebook\FacebookBatchResponse $response */
        foreach ( $responses as $key => $response ) {

            // Page limit rate error
            if ( $responses->isError() && $response->getThrownException()->getCode() == self::FB_ERROR_PAGE_LIMIT_RATE ) {
                $this->addFlashBag('error', 'result.page.error.rate_limit', array('%name%' => $agency->getName()));
            }

            if ( $key === 'update-picture' ) {
                if ( $response->isError() ) {
                    $data = [
                        'Agency ID : '.$agency->getId(),
                        'Image ID : '.$picture->getId()
                    ];
                    $this->logError($response->getThrownException(), 'updatePage', 'facebook/update_page.log', $data);
                    $this->addFlashBag('error', 'result.page.error.picture', array('%name%' => $agency->getName()));
                }
                else {
                    $this->addFlashBag('success', 'result.page.success.picture', array('%name%' => $agency->getName()));
                }
            }

            if ( $key === 'update-page' ) {
                $persist = false;

                foreach ( $params as $paramName => $value ) {

                    if ( $response->isError() ) {
                        $data = [
                            'Agency ID : '.$agency->getId(),
                            ucfirst($paramName).' : '.$value
                        ];
                        $this->logError($response->getThrownException(), 'updatePage', 'facebook/update_page.log', $data);
                        $this->addFlashBag('error', 'result.page.error.'.$paramName, array('%name%' => $agency->getName()));
                    }
                    else {
                        $this->addFlashBag('success', 'result.page.success.'.$paramName, array('%name%' => $agency->getName()));

                        // Update agency entity except for cover
                        if ( $paramName !== 'cover' ) {
                            $agency->{'set'.ucfirst($paramName)}($value);
                            $persist = true;
                        }
                    }

                }

                if ( $persist ) {
                    $this->em->persist($agency);
                    $this->em->flush();
                }
            }
        }
    }


    /**
     * Create the assets album for one agency (if there is none) and add the new photo
     * return the picture facebook data
     *
     * @param Agency $agency
     * @param Image $cover
     * @param string $pageToken
     * @return mixed
     */
    private function addCoverToAssetsAlbum(Agency $agency, Image $cover, $pageToken, $secondChance = true)
    {
        $facebookCover = null;
        $url = $this->container->getParameter('base_url').$cover->getWebPath();
        try {
            // Create album if not created yet
            $albumId = $agency->getAssetsAlbum();
            if ( !$albumId ) {
                $albumId = $this->createAlbum( $agency, $pageToken );
            }

            // Add cover to album
            $facebookCover = json_decode(
                $this->fb->post(
                    '/'.$albumId.'/photos',
                    [
                        'url' => $url,
                        'no_story' => true
                    ],
                    $pageToken
                )->getBody()
            );

            $this->addFlashBag('success', 'result.page.success.coverAlbum', array('%name%' => $agency->getName()));
        } catch (\Exception $e) {
            $data = [
                'Agency ID : '.$agency->getId(),
                'Album ID : '.$agency->getAssetsAlbum(),
                'Image URL : '.$url
            ];

            // If Facebook album does not exist or in case of any error, we re-create one
            if ( $secondChance ) {
                $this->logError($e, 'addPhotoToAssetsAlbum', 'facebook/add_photo_to_assets_album.log', array_merge($data, ['2ND CHANCE : Trying to create a new album...']));

                $agency->setAssetsAlbum(null);
                $this->addCoverToAssetsAlbum( $agency, $cover, $pageToken, false );
            }
            else {
                $this->logError($e, 'addPhotoToAssetsAlbum', 'facebook/add_photo_to_assets_album.log', array_merge($data, ['2ND CHANCE FAILED ']));
                $this->addFlashBag('error', 'result.page.error.coverAlbum', array('%name%' => $agency->getName()));
            }

        }
        return $facebookCover;
    }

    /**
     * @param Agency $agency
     * @param $pageToken
     * @return string
     */
    private function createAlbum(Agency $agency, $pageToken)
    {
        $assetAlbum = json_decode(
            $this->fb->post(
                '/'.$agency->getPageId().'/albums',
                [ 'name' => Agency::ASSETS_ALBUM_NAME ],
                $pageToken
            )->getBody()
        );
        $agency->setAssetsAlbum($assetAlbum->id);
        $this->em->persist($agency);
        $this->em->flush();

        return $agency->getAssetsAlbum();
    }

    /**
     * @param \Exception $e
     * @param $name
     * @param $path
     * @param array $data
     * @param array $data
     */
    private function logError(\Exception $e, $name, $path, array $data = [])
    {
        $log = new Logger($name);

        $handler = new StreamHandler($this->container->get('kernel')->getRootDir().'/logs/'.$path, Logger::ERROR);
        $handler->setFormatter(new ColoredLineFormatter());

        $log->pushHandler($handler);
        $log->error($e->getCode());
        $log->error($e->getMessage());

        foreach ( $data as $value ) {
            $log->error($value);
        }
        
        $log->error('-----------------------');
    }

    /**
     * Add FlashBag
     *
     * @param $status
     * @param $translation
     * @param $params
     */
    public function addFlashBag($status, $translation, $params)
    {
        $this->container->get('session')->getFlashBag()->add(
            $status,
            $this->translator->trans(
                $translation,
                $params
            )
        );
    }
}
